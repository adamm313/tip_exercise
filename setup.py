from setuptools import setup

setup(
    name='tip_cli',
    version='0.1',
    py_modules=['tip_cli'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        tip_cli=tip_cli:cli
    ''',
)