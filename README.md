# Description - tip_cli 
A CLI that generates a tree structure of year, month, day directories in the CWD, based on the date range provided.  .keep files are created in every year, month, and day directory if the optional 'keep' argument is enabled.

# Assumptions
Environment has python 3 and pip already installed, appropriate permissions  
User has appropriate permissions (r/w access) to filesystem

# Possible Improvements
1)  Unit tests added, integrated with Gitlab CI Pipeline and triggered on merges to master branch
2)  Flexibility in date parsing (mm/dd/yyyy only format supported currently)
3)  Verbose logging option
# Arguments
--start_date (required): Start of date range (inclusive), in mm/dd/yyyy format. Ex: 1/2/2017, 01/02/2017  
--end_date (required): End of date range (inclusive), in mm/dd/yyyy format.  Ex:  2/1/2017, 02/01/2017  
--keep (optional):  Creates .keep file in each and every directory if enabled. Default value is set to False.  Ex: True, 1

# Installation
1)  Clone tip_exercise repository  
git clone https://gitlab.com/adamm313/tip_exercise.git
2) CLI pip to install CLI
pip install .

# Usage
1)  Display help documentation  
tip_cli --help

2)  Create year, month, day directories for entire year of 2017 (without .keep files)  
tip_cli --start_date 1/1/2017 --end_date 12/31/2017

3)  Create year, month, day directories for entire year of 2017 (with .keep files)  
tip_cli --start_date 1/1/2017 --end_date 12/31/2017 --keep True


# Error Handling
1)  Malformed start_date and/or end_date  
tip_cli --start_date 111/1/2017 --end_date 12/31/2017  
tip_cli --start_date 1/1/2017 --end_date 121/31/2017  
tip_cli --start_date 1-1-2017 --end_date 12-31-2017  

2)  Bad date range  
tip_cli --start_date 12/31/2017 --end_date 1/1/2017  


