import click
import os
import shutil
from datetime import datetime as dt
from datetime import timedelta as td


#Checks if dt_str can be converted to a datetime object using the date format provided.
def is_valid_dt(dt_str, format):
    try:
        dt.strptime(dt_str,format)
        return 1
    except:
        return 0

#Converts dt_str to a datetime object using the date format provided.
def str_to_dt(dt_str, format):
        return dt.strptime(dt_str,format)

#Checks if a date range is valid, i.e., end_dt has to occur after start_dt.
def is_valid_date_range(start_dt, end_dt):
    if end_dt > start_dt:
        return 1
    else:
        return 0

#Parses the year, month, and day from a datetime object.
def parse_dt(dt):
    year = str(dt.year) 
    month = str(dt.month)
    day = str(dt.day)
    return year,month,day

#A generator that yields the year, month, and day for all datetime objects in the date range provided (inclusive).
def generate_dt_parts(start_dt, end_dt):
    for n in range(int((end_dt - start_dt).days + 1)):
        curr_dt = start_dt + td(n)
        year,month,day = parse_dt(curr_dt)
        yield year,month,day

#Creates absolute paths, independent of operating system, for year, month, and day directories that need to be created.
def create_paths(cwd,year,month,day):
    year_path = os.path.join(cwd,year)
    month_path = os.path.join(cwd,year,month)
    day_path = os.path.join(cwd,year,month,day)
    return year_path,month_path,day_path

#Implements .keep file handling at year, month, day level depending value of 'keep'
def keep_file(path,keep):
    keep_path = os.path.join(path,".keep")
    #Touch .keep file if keep = True
    if keep:
        click.echo('creating file %s' % keep_path)
        open(keep_path,'a').close()
    #Try to remove .keep file if keep = False
    else:
        try:
            os.remove(keep_path)
            click.echo('removing file %s' % keep_path)
        except FileNotFoundError:
            click.echo('%s not found, nothing to remove' % keep_path)

#Creates year, month, day directories in date range provided and adds or removes .keep files from
#directories depending on value of 'keep'
def create_directories(cwd,start_dt,end_dt,keep=False):
    #date counter
    i = 0
    for year,month,day in generate_dt_parts(start_dt, end_dt):
        #Handling for first date in date range
        if i == 0:
            last_year = year
            last_month = month
        #Create year, month, day paths for date in date range
        year_path,month_path,day_path = create_paths(cwd,year,month,day)
        #Remove day directory and contents if it already exists
        if os.path.exists(day_path):
            click.echo('directory %s already exists, removing directory and its contents' % day_path)
            shutil.rmtree(day_path)
        #Create day directory
        click.echo('creating directory %s' % day_path)
        os.makedirs(day_path)
        #.keep file handling for year directories
        if year != last_year or i == 0:
            keep_file(year_path,keep)
        last_year = year
        #.keep file handling for month directories
        if month != last_month or i == 0:
            keep_file(month_path,keep)
        last_month = month
        #.keep file handling for day directories   
        keep_file(day_path,keep)
        #increment date counter
        i += 1

#Used to notify the user when there is a risk that data will be overwritten if they continue executing the program
def directories_exist(cwd,start_dt,end_dt):
    existing_directories = []
    for year,month,day in generate_dt_parts(start_dt, end_dt):
        year_path,month_path,day_path = create_paths(cwd,year,month,day)
        if os.path.exists(day_path):
            existing_directories.append(day_path)
    return existing_directories

#Decorators that up required arguments 'start_date', 'end_date' and optional argument 'keep'
@click.command()
@click.option('--start_date', required=True, help='Start of date range (inclusive), in mm/dd/yyyy format. Ex: 1/2/2017, 01/02/2017')
@click.option('--end_date', required=True, help='End of date range (inclusive), in mm/dd/yyyy format.  Ex:  2/1/2017, 02/01/2017')
@click.option('--keep', type=bool, required=False, default=False, help='Creates .keep file in each and every directory if enabled. \
Default value is set to False.  Ex: True, 1')

#Driver function
def cli(start_date, end_date, keep):
    """A CLI that generates a tree structure of year, month, day directories based on the date range provided.  
    .keep files are created in every year, month, and day directory if the optional 'keep' argument is enabled."""
    
    #sets date format to be used (mm/dd/yyyy)
    fmt = '%m/%d/%Y'
    #sets current working directory path
    cwd = os.getcwd()

    #validate start_date argument
    if is_valid_dt(start_date, format=fmt):
        click.echo('start_date %s has been successfully validated...' % start_date)
    else:
        click.echo('Invalid string %s has been passed for start_date, please ensure that start_date is in the mm/dd/yyyy format...' % start_date)
        raise click.Abort()
        
    #validate end_date argument
    if is_valid_dt(end_date,format=fmt):
        click.echo('end_date %s has been successfully validated...' % end_date)
    else:
        click.echo('Invalid string %s has been passed for end_date, please ensure that end_date is in the mm/dd/yyyy format...' % end_date)
        raise click.Abort()
    
    #convert date arguments to datetime objects after validation
    start_dt = str_to_dt(start_date,format=fmt)
    end_dt = str_to_dt(end_date,format=fmt)

    #validate date range
    if is_valid_date_range(start_dt,end_dt):
        click.echo('date range %s - %s has been succesfully validated...' % (start_date, end_date))
    else:
        click.echo('Invalid date range %s - %s has been passed.  Please ensure that end_date is greater than or equal to start_date.' % (start_date, end_date))
        raise click.Abort()

    #validate keep argument
    if keep:
        click.echo('keep argument validated as True, all directories will be created with a .keep file...')
    else:
        click.echo('keep argument validated as False, all directories will be created without a .keep file...')

    #check if directories already exist in date range already exist and confirm that user would like to overwrite their contents
    if directories_exist(cwd,start_dt,end_dt):
        click.echo('One or more directories in this date range already exist and may contain data files...')
        click.echo('Continuing will OVERWRITE the contents of these directories...')
        if click.confirm('Would you like to continue?'):
            click.echo('Overwriting existing directories...')
        else:
            raise click.Abort()

    #create directories
    try:
        create_directories(cwd,start_dt,end_dt,keep)
    except:
        click.echo('A permissions issue has been encountered when trying to create or a .keep file or directory.')  
        click.echo('Please ensure that you have appropriate access and have closed any directories that may be overwritten, then re-run the program.')
        raise click.Abort()
    click.echo('Done!')


if __name__ == '__main__':
    cli()